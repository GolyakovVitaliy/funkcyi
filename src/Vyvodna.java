import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Vyvodna {
    private BufferedReader reader;

    public void bufferden() {

        int den=0;
        reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите день недели");
            den = Integer.parseInt(reader.readLine());
            switch (den) {
                case 1:
                    System.out.println("Понедельник");
                    break;
                case 2:
                    System.out.println("Вторник");
                    break;
                case 3:
                    System.out.println("Среда");
                    break;
                case 4:
                    System.out.println("Четверг");
                    break;
                case 5:
                    System.out.println("Пятница");
                    break;
                case 6:
                    System.out.println("Суббота");
                    break;
                case 7:
                    System.out.println("Воскресенье");
                    break;
                default:
                    System.out.println("Дня с таким кодом нет. Повторите ввод");
                    bufferden();
                    break;
            }
        } catch (NumberFormatException n) {
            System.out.println("Нужно ввести число");
            bufferden();
        } catch (IOException e) {
            e.printStackTrace();
        }
      }

    public void bufferchislo() {

        String chislo="";
        reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("А теперь введите число от 0 до 999");
            chislo = reader.readLine();
            if (chislo.length()==1) {chislo="00"+chislo; }
            if (chislo.length()==2) {chislo="0"+chislo; }
            if (chislo.length()>3) {chislo=chislo.substring(0, 3);
                System.out.println("Нет, только до 999");}
            switch (chislo.charAt(0)) {
                case '0':
                    break;
                case '1':
                    System.out.print(" сто");
                    break;
                case '2':
                    System.out.print(" двести");
                    break;
                case '3':
                    System.out.print(" триста");
                    break;
                case '4':
                    System.out.print(" четыреста");
                    break;
                case '5':
                    System.out.print(" пятьсот");
                    break;
                case '6':
                    System.out.print(" шестьсот");
                    break;
                case '7':
                    System.out.print(" семьсот");
                    break;
                case '8':
                    System.out.print(" восемьсот");
                    break;
                case '9':
                    System.out.print(" девятьсот");
                    break;
            }
            switch (chislo.charAt(1)) {
                case '0':
                    break;
                case '1':
                    System.out.print(" десять");
                    break;
                case '2':
                    System.out.print(" двадцать");
                    break;
                case '3':
                    System.out.print(" тридцать");
                    break;
                case '4':
                    System.out.print(" сорок");
                    break;
                case '5':
                    System.out.print(" пятьдесят");
                    break;
                case '6':
                    System.out.print(" шестьдесят");
                    break;
                case '7':
                    System.out.print(" семьдесят");
                    break;
                case '8':
                    System.out.print(" восемьдесят");
                    break;
                case '9':
                    System.out.print(" девяносто");
                    break;
            }

            switch (chislo.charAt(2)) {
                case '0':
                    break;
                case '1':
                    System.out.print(" один");
                    break;
                case '2':
                    System.out.print(" два");
                    break;
                case '3':
                    System.out.print(" три");
                    break;
                case '4':
                    System.out.print(" четыре");
                    break;
                case '5':
                    System.out.print(" пять");
                    break;
                case '6':
                    System.out.print(" шесть");
                    break;
                case '7':
                    System.out.print(" семь");
                    break;
                case '8':
                    System.out.print(" восемь");
                    break;
                case '9':
                    System.out.print(" девять");
                    break;

            }
        } catch (NumberFormatException n) {
            System.out.println("Нужно ввести число");
            bufferchislo();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    }

